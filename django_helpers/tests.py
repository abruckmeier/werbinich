import django



class Test_Setup__Database_Integrity:
	"""
	Provide a save, update and error class function for database integrity tests (opposite to Django validation tests)
	"""

	def save(self, cls, **kwargs):
		"""
		Save method (to override for other tests (Django validation)) for data to database
		:param cls: class of model to save to
		:param kwargs: arguments to save as key-value-pair
		:return: saved instance of model
		"""
		return cls.objects.create(
			**kwargs,
		)

	def update(self, inst):
		"""
		Update method to modify existing entry
		:param inst: model instance to save
		:return: None
		"""
		inst.save()

	def expected_error_class(self):
		"""
		Returns expected error class on errors. Method to be overwritten, when performing Django validation.
		:return: error class
		"""
		return (django.db.utils.InternalError, django.db.utils.IntegrityError, django.db.utils.DataError)



class Test_Setup__Django_Validation:
	"""
	Provide a save, update and error class function for Django validation tests (opposite to database integrity tests)
	"""

	def save(self, cls, **kwargs):
		"""
		Save method for Django validation with clean method to have validation errors
		:param cls: class of model to save to
		:param kwargs: arguments to save as key-value-pair
		:return: saved instance of model
		"""
		inst = cls(
			**kwargs
		)
		inst.full_clean()
		inst.save()
		return inst

	def update(self, inst):
		"""
		Update method to modify existing entry
		:param inst: model instance to save
		:return: None
		"""
		inst.full_clean()
		inst.save()

	def expected_error_class(self):
		"""
		Returns expected error class on errors. Method to be overwritten, when performing Django validation.
		:return: error class
		"""
		return django.core.exceptions.ValidationError

