from django.shortcuts import render, HttpResponseRedirect, reverse, HttpResponse
from django.views import View
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.db import transaction
from django.template.loader import render_to_string
from types import SimpleNamespace
from django.http import JsonResponse
from django.utils import timezone

from . import models
from . import utils
from . import forms
from . import game as game_module



# Start Page
class Start_Page(View):

	def get(self, request):
		request.session.set_test_cookie()
		return render(request, 'werbinich/start.html',)


	def post(self, request):

		if not request.session.test_cookie_worked():
			messages.add_message(request, messages.ERROR, 'You need to enable cookies to make this site work for you.')
			return render(request, 'werbinich/start.html')


		game = models.Game(
			host_session_key= request.session.session_key,
			game_url= utils.generateRandomString(length=20),
		)
		game.save()

		messages.add_message(request, messages.SUCCESS, 'You have started a game. Now invite your friends by sending them the link of this page.')

		return HttpResponseRedirect(reverse('game_page', kwargs= {'hash': game.game_url}))



class Game_Page(View):

	def _get_game(self, hash):

		games = models.Game.objects.filter(
			game_url= hash,
		)
		if games.count() != 1:
			return None

		return games[0]


	def _get_player(self, request, game):

		player = game.player_set.filter(player_session_key= request.session.session_key)
		if player: return player[0]
		else: return None



	def get(self, request, hash):
		if request.GET.get('action')=='get_player_section': return self._get_player_section(request, hash)
		else:
			return self._get_default_page(request, hash)



	def _get_player_section(self, request, hash):

		game = self._get_game(hash)
		if not game:
			messages.add_message(request, messages.ERROR, 'This game does not exist.')
			return JsonResponse(dict(
				main_switch= False,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= True,
				message= None,
			))

		if game.ts_ended:
			messages.add_message(request, messages.ERROR, 'This game has already ended.')
			return JsonResponse(dict(
				main_switch= False,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= True,
				message= None,
			))

		# Game is running: All names to guess have been assigned
		if game.is_running():
			return JsonResponse(dict(
				main_switch= True,
				failed= False,
				content_main= render_to_string(
					'werbinich/player_main_section_3_listing.html',
					dict(
						game= game,
					),
					request=request,
				),
				content_input= None,
				back_to_start= False,
				message= None,
			))

		# User has assigned the name to guess
		player = self._get_player(request, game)
		all_players = models.Player.objects.filter(
				game__game_url= hash,
			)

		# Game has started
		if game.ts_started:
			if request.session.get(f'{game.game_url}_sent_name_assignment_input_form', False):
				content_input = None
			else:
				request.session[f'{game.game_url}_sent_name_assignment_input_form'] = True
				content_input = render_to_string(
					'werbinich/player_input_section_2_enter_werbinich_name.html',
					dict(
						player= player,
						name_assignment_form= forms.Name_Assignment_Form(),
					),
					request=request,
				)

			return JsonResponse(dict(
				main_switch= True,
				failed= False,
				content_main= render_to_string(
					'werbinich/player_main_section_2_enter_werbinich_name.html',
					dict(
						player= player,
						all_players= all_players,
					),
					request=request,
				),
				content_input= content_input,
				back_to_start= False,
				message= None,
			))

		# Game not has started
		else:
			return JsonResponse(dict(
				main_switch= True,
				failed= False,
				content_main= render_to_string(
					'werbinich/player_main_section_1_game_not_started.html',
					dict(
						player= player,
						all_players= all_players,
					),
					request=request,
				),
				content_input= None,
				back_to_start= False,
				message= None,
			))



	def _get_default_page(self, request, hash):
		request.session.set_test_cookie()

		game = self._get_game(hash)
		if not game:
			messages.add_message(request, messages.WARNING, 'This game does not exist.')
			return HttpResponseRedirect(reverse('start_page'))

		if game.ts_ended:
			messages.add_message(request, messages.ERROR, 'This game has already ended.')
			return HttpResponseRedirect(reverse('start_page'))

		if request.session.session_key == game.host_session_key:
			user_is_host = True
		else:
			user_is_host = False

		if game.ts_started: game_has_started = True
		else: game_has_started = False


		all_players = models.Player.objects.filter(
			game__game_url= hash,
		)
		player = self._get_player(request, game)

		player_register_form = forms.Player_Register_Form()
		name_assignment_form = forms.Name_Assignment_Form()


		return render(request, 'werbinich/game.html', dict(
			game= game,
			user_is_host= user_is_host,
			game_has_started= game_has_started,
			all_players= all_players,
			player= player,
			player_register_form= player_register_form,
			name_assignment_form= name_assignment_form,
			domain= get_current_site(request).domain,
			protocol_secure= request.is_secure(),
		))



	@transaction.atomic
	def post(self, request, hash):
		if not request.session.test_cookie_worked():
			messages.add_message(request, messages.ERROR, 'You need to enable cookies to make this site work for you.')
			return HttpResponseRedirect(reverse('start'))

		if request.POST.get('action')=='player_register': return self._post_player_register(request, hash)
		if request.POST.get('action')=='assign_name': return self._assign_name(request, hash)
		elif request.POST.get('action')=='start_game': return self._post_start_game(request, hash)
		elif request.POST.get('action')=='end_game': return self._post_end_game(request, hash)
		else:
			return HttpResponse(status=406)


	def _assign_name(self, request, hash):

		try: request.session.pop(f'{game.game_url}_sent_name_assignment_input_form')
		except: pass

		game = self._get_game(hash)
		if not game:
			messages.add_message(request, messages.ERROR, 'This game does not exist.')
			return JsonResponse(dict(
				main_switch= False,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= True,
				message= None,
			))

		if game.ts_ended:
			messages.add_message(request, messages.ERROR, 'This game has already ended.')
			return JsonResponse(dict(
				main_switch= False,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= True,
				message= None,
			))

		player = self._get_player(request, game)
		all_players = models.Player.objects.filter(
			game__game_url= hash,
		)
		if not player or not game.ts_started :
			return JsonResponse(dict(
				main_switch= True,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= False,
				message= 'Game not started or no valid player',
			))

		form = forms.Name_Assignment_Form(request.POST)
		if form.is_valid():
			player.player_to_assign.werbinich_name = form.cleaned_data['name']
			player.player_to_assign.save()

			return JsonResponse(dict(
				main_switch= True,
				failed= False,
				content_main= render_to_string(
					'werbinich/player_main_section_2_enter_werbinich_name.html',
					dict(
						player= player,
						all_players= all_players,
					),
					request=request,
				),
				content_input= render_to_string(
					'werbinich/player_input_section_2_enter_werbinich_name.html',
					dict(
						player= player,
						name_assignment_form= form,
					),
					request=request,
				),
				back_to_start= False,
				message= None,
			))

		return JsonResponse(dict(
			main_switch= True,
			failed= False,
			content_main= render_to_string(
				'werbinich/player_main_section_2_enter_werbinich_name.html',
				dict(
					player= player,
					all_players= all_players,
				),
				request=request,
			),
			content_input= render_to_string(
				'werbinich/player_input_section_2_enter_werbinich_name.html',
				dict(
					player= player,
					name_assignment_form= form,
				),
				request=request,
			),
			back_to_start= False,
			message= None,
		))



	def _post_start_game(self, request, hash):

		failed = False
		back_to_start = False
		content = None
		message = None

		game = self._get_game(hash)
		if not game:
			failed = True
			back_to_start = True
			messages.add_message(request, messages.ERROR, 'This game does not exist.')

		if not failed:
			if game.ts_ended:
				failed = True
				back_to_start = True
				messages.add_message(request, messages.ERROR, 'This game has already ended.')

		if not failed:
			if game.ts_started:
				failed = True
				back_to_start = True
				messages.add_message(request, messages.ERROR, 'This game has already started.')

		if not failed:
			savepoint = transaction.savepoint()
			game.ts_started = timezone.now()
			game.save()

			succ, reason = game_module.connectUsersToAssignNames(game)
			failed = not succ
			if failed:
				message = reason
				transaction.savepoint_rollback(savepoint)
			else:
				transaction.savepoint_commit(savepoint)

		if not failed:
			content= render_to_string(
				'werbinich/host_section_game_has_started.html',
				request=request,
			)

		return JsonResponse(dict(
			failed= failed,
			content= content,
			back_to_start= back_to_start,
			message= message,
		))



	def _post_end_game(self, request, hash):

		game = self._get_game(hash)
		if not game:
			messages.add_message(request, messages.ERROR, 'This game does not exist.')
			return JsonResponse(dict(
				main_switch= False,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= True,
				message= None,
			))

		if game.ts_ended:
			messages.add_message(request, messages.ERROR, 'This game has already ended.')
			return JsonResponse(dict(
				main_switch= False,
				failed= True,
				content_main= None,
				content_input= None,
				back_to_start= True,
				message= None,
			))

		game.ts_ended = timezone.now()
		game.save()

		messages.add_message(request, messages.SUCCESS, 'This game has been ended.')
		return JsonResponse(dict(
			main_switch= False,
			failed= True,
			content_main= None,
			content_input= None,
			back_to_start= True,
			message= 'This game has been ended',
		))



	def _post_player_register(self, request, hash):

		failed = False
		player = None
		player_register_form = forms.Player_Register_Form()

		game = self._get_game(hash)
		if not game:
			failed = True
			messages.add_message(request, messages.ERROR, 'This game does not exist.')

		if not failed:
			if game.ts_ended:
				failed = True
				messages.add_message(request, messages.ERROR, 'This game has already ended.')

		if not failed:
			if game.ts_started:
				failed = True
				messages.add_message(request, messages.ERROR, 'This game has already started.')

		if not failed:
			all_players = models.Player.objects.filter(
				game__game_url= hash,
			)
			players = all_players.filter(
				player_session_key= request.session.session_key,
			)
			if players.count() == 0:
				player = None
			else:
				player = players[0]

		if not failed and not player:
			updated_data = request.POST.copy()
			updated_data.update(dict(
				game = game.id,
				player_session_key= request.session.session_key,
			))
			player_register_form = forms.Player_Register_Form(data= updated_data)
			if player_register_form.is_valid():
				player = player_register_form.save(commit=False)
				player.save()

		all_players = models.Player.objects.filter(
			game__game_url= hash,
		)

		return JsonResponse(dict(
			failed= failed,
			content_input= render_to_string(
				'werbinich/player_input_section_1_game_not_started.html',
				dict(
					player= player,
					player_register_form= player_register_form,
					all_players= all_players,
				),
				request=request,
			),
			content_main= render_to_string(
				'werbinich/player_main_section_1_game_not_started.html',
				dict(
					player= player,
					all_players= all_players,
				),
				request=request,
			),
		))



