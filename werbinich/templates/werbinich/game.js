function assign_name() {

	var form = $('form#name_assignment_form');

	$.ajax({
		type: "POST",
		url: '{% url "game_page" hash=game.game_url %}',
		data: form.serialize() + '&action=assign_name',
		error: function(jqXHR, textStatus, errorThrown){
					var msg = 'Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
					$('#player_section_messages').html(msg);
					setTimeout(function() {
					  $('#player_section_messages').html('<br>');
					}, 10000);
		},
		success: function(data) {
			if (data.failed) {
			  window.location.href = '{% url "start_page" %}';
			  return false;
			} else {
			  $('#player_input_section').html(data.content_input);
			  $('#player_main_section').html(data.content_main);
			}
		}
	});
}

function register_player() {

	var form = $('form#player_register_form');

	$.ajax({
		type: "POST",
		url: '{% url "game_page" hash=game.game_url %}',
		data: form.serialize() + '&action=player_register',
		error: function(jqXHR, textStatus, errorThrown){
					var msg = 'Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
					$('#player_section_messages').html(msg);
					setTimeout(function() {
					  $('#player_section_messages').html('<br>');
					}, 10000);
		},
		success: function(data) {
			if (data.failed) {
			  window.location.href = '{% url "start_page" %}';
			  return false;
			} else {
			  $('#player_input_section').html(data.content_input);
			  $('#player_main_section').html(data.content_main);
			}
		}
	});
}

var main_switch_update_player_section = true;
function update_player_section() {

	if (!main_switch_update_player_section) {return;}

	$.ajax({
		type: "GET",
		url: '{% url "game_page" hash=game.game_url %}',
		data: {'action': 'get_player_section',},
		error: function(jqXHR, textStatus, errorThrown){
					var msg = 'Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
					$('#player_section_messages').html(msg);
					setTimeout(function() {
					  $('#player_section_messages').html('<br>');
					}, 10000);
		},
		success: function(data) {
			main_switch_update_player_section = data.main_switch;

			if (data.failed && data.back_to_start) {
			  window.location.href = '{% url "start_page" %}';
			  return false;
			} else if (data.failed && !data.back_to_start) {
				$('#player_section_messages').html(data.message);
				setTimeout(function() {
				  $('#player_section_messages').html('<br>');
				}, 10000);
			} else if (!data.failed) {
				if (data.content_main) {
					$('#player_main_section').html(data.content_main);
				}
				if (data.content_input) {
					$('#player_input_section').html(data.content_input);
				}
			} else {
				$('#player_section_messages').html('Internal Error');
				setTimeout(function() {
				  $('#player_section_messages').html('<br>');
				}, 10000);
			}
		}
	});
}

var interval_ID_update_player_section = window.setInterval(function(){
	update_player_section();
}, 2000);
$(window).focus(function(){
	update_player_section();
	if (interval_ID_update_player_section) {
	  window.clearInterval(interval_ID_update_player_section);
	}
	interval_ID_update_player_section = window.setInterval(function(){
		update_player_section();
	}, 2000);
});
$(window).blur(function(){
	window.clearInterval(interval_ID_update_player_section);
});



function start_game() {

	$.ajax({
		type: "POST",
		url: '{% url "game_page" hash=game.game_url %}',
		data: {'action': 'start_game'},
		error: function(jqXHR, textStatus, errorThrown){
					var msg = 'Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
					$('#game_administration_messages').html(msg);
					setTimeout(function() {
					  $('#game_administration_messages').html('<br>');
					}, 10000);
		},
		success: function(data) {
			if (data.failed && data.back_to_start) {
			  window.location.href = '{% url "start_page" %}';
			  return false;
			} else if (data.failed && !data.back_to_start) {
				$('#game_administration_messages').html(data.message);
				setTimeout(function() {
				  $('#game_administration_messages').html('<br>');
				}, 10000);
			} else if (!data.failed) {
				$('#host_section').html(data.content);
			} else {
				$('#game_administration_messages').html('Internal Error');
				setTimeout(function() {
				  $('#game_administration_messages').html('<br>');
				}, 10000);
			}
		}
	});
}


function end_game() {

	$.ajax({
		type: "POST",
		url: '{% url "game_page" hash=game.game_url %}',
		data: {'action': 'end_game'},
		error: function(jqXHR, textStatus, errorThrown){
					var msg = 'Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
					$('#game_administration_messages').html(msg);
					setTimeout(function() {
					  $('#game_administration_messages').html('<br>');
					}, 10000);
		},
		success: function(data) {
			if (data.failed && data.back_to_start) {
			  window.location.href = '{% url "start_page" %}';
			  return false;
			} else if (data.failed && !data.back_to_start) {
				$('#game_administration_messages').html(data.message);
				setTimeout(function() {
				  $('#game_administration_messages').html('<br>');
				}, 10000);
			} else if (!data.failed) {
				$('#host_section').html(data.content);
			} else {
				$('#game_administration_messages').html('Internal Error');
				setTimeout(function() {
				  $('#game_administration_messages').html('<br>');
				}, 10000);
			}
		}
	});
}
