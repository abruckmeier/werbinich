from django import forms

from . import models


class Player_Register_Form(forms.ModelForm):

	class Meta:
		model = models.Player
		fields = ('player_name','game', 'player_session_key',)
		labels = {
			'player_name': 'Spielername',
		}
		help_texts = {
		}
		widgets = {
			'game': forms.HiddenInput(),
			'player_session_key': forms.HiddenInput(),
		}


class Name_Assignment_Form(forms.Form):

	name = forms.CharField()