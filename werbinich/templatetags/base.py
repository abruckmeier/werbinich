from django import template
register = template.Library()


from django.conf import settings


# Function to display the site name of the settings in the templates
@register.simple_tag
def get_site_name():
    return getattr(settings,'SITE_NAME')