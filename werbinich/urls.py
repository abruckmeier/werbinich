from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required

from . import views



urlpatterns = [
	url(r'^$',
		views.Start_Page.as_view(),
		name='start_page'
	),

	url(r'^play/(?P<hash>[a-zA-Z0-9]+)/$',
		views.Game_Page.as_view(),
		name='game_page'
	),
]