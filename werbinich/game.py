
import os
import sys
import django

if __name__ == '__main__':
	# Setup the Django environment of the flex platform
	BASE = os.path.dirname(os.path.dirname((os.path.abspath(__file__))))
	sys.path.append(BASE)
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "werbinich_project.settings")
	django.setup()



from random import SystemRandom



def connectUsersToAssignNames(game):
	""" When the host starts the game, the users will be connected, so that each user has to assign a werbinich name to the other user.

	:param game: Game instance.
	:return: Boolean. Indicator for success
	"""

	if not game.ts_started: return False, 'Game has not been set as started'
	if game.ts_ended: return False, 'Game has already ended'

	players = game.player_set.all()
	if players.count() < 2: return False, 'There need to be at least two players within the game'
	if players.filter(player_to_assign__isnull=False).exists(): return False, 'An connection has already been conducted'

	# Randomly create an order
	players_id_list = [ p.id for p in players ]
	SystemRandom().shuffle(players_id_list)

	# Assign the next neighbour to set a werbinich name
	to_assign_list = players_id_list[1:] + [players_id_list[0]]

	for idx, _player in enumerate(players_id_list):
		_p = players.get(id=players_id_list[idx])
		_p.player_to_assign = players.get(id=to_assign_list[idx])
		_p.save()

	return True, 'ok'


if __name__=='__main__':

	from werbinich import models

	game = models.Game.objects.get(id=19)

	print(connectUsersToAssignNames(game))