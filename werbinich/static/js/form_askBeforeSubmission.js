/*
	Before submission of a form / formset, the user is asked if he is sure to do so.
	Usage:
		<form
			method="POST"
			...
			onsubmit="window.askBeforeSubmission(event)"
		>
			{{ form }}
		</form>
*/


window.askBeforeSubmission = function(event) {
	var isValid = confirm('Sind Sie sicher, diese Änderungen durchzuführen?');
	if (!isValid) {
		event.preventDefault();
		return false;
	}
}
