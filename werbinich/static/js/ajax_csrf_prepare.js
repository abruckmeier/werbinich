/*
	For the ajax-POST-requests, one needs to set the csrf Token.
	This is needed, when POST-requests are performed in Modals, which are loaded with ajax-GET-requests and thus those forms do not contain the correct csrf token.
*/

		var csrftoken;

		function csrfSafeMethod(method) {
	    	// these HTTP methods do not require CSRF protection
	    	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}

		$(document).ready(function(){
			csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
			}
		);
		
		$(document).ready(function(){
			$.ajaxSetup({
		    	beforeSend: function(xhr, settings) {
		        	if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
		            	xhr.setRequestHeader("X-CSRFToken", csrftoken);
		        	}
		    	}
			});
		});
		