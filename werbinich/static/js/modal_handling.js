// Functions to fill, update and empty modals. Use this functions with get- and post-requests


	/**
	 * Fill the Modal with data from a successful ajax request.
	 * @param {object} data - 	Dict-Data from the webserver with the fields `modalLabel`, `modalBody` and `modalFooter`.
	 */
	function fillModal(data) {
		$("h4#modalLabel").html(data['modalLabel']);
		$("div#modalBody").html(data['modalBody']);
		$("div#modalFooter").html(data['modalFooter']);
	}


	/**
	 * Empty the Modal content.
	 */
	function emptyModal() {
		$("h4#modalLabel").html('');
		$("div#modalBody").html('');
		$("div#modalFooter").html('');
	}


	/**
	 * Fill in the modal data or close and reload page after a successful ajax request.
	 * Keyword for closing modal and reloading page is `success` in the data set to `true`
	 * @param {object} data - Dict-Data from the webserver with the fields `modalLabel`, `modalBody` and `modalFooter`.
	 * @param {boolean} reload_on_success - Optional. If `true`, the page will be reloaded. (Important, when a POST in a Modal happens and and changes data on the main page.)
	 */
	function updateOrCloseModal(data, reload_on_success=false) {

		// Success does not need to be in the data variable for being false. Only true, when existent and true
		if (data['success'] === true) {
			// On success, clear the modal and close it
			emptyModal()
			$("div#modal").modal('hide');

			// Reload, so the changes on the main page will be visible. 
			if (reload_on_success) {
				location.reload({'forceGet':true});
			}
		}
		else {
			// Write to modal if no success on saving form.
			fillModal(data);
			$("div#modal").modal('show');
		}
	}


	/**
	 * Request GET-data from the webserver for the modal.
	 * Write the data to the modal. Either open or update.
	 * @param {string} what - Keyword, the webserver expects to process the correct modal request.
	 * @param {integer} id - Optional. Pass the id to the webserver with the GET-data
	*/
	function getRequestForModal(what, id) {
		var _data;

		if (id === undefined) {
			_data = {'what': what};
		} else {
			_data = {'what': what, 'id': id};
		}
		$.ajax({
			type: 'GET',
			url: window.location.href,
			data: _data,
			error: function(jqXHR, textStatus, errorThrown){
				var msg = 'GET-Request: Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
				console.log(msg);
				updateOrCloseModal({'modalBody': msg})
			},
			success: function(data){
				updateOrCloseModal(data);
			}
		})
	}


	/**
	 * Request GET-data from the webserver for the modal by requesting a specific URL (not as above, an url with further data).
	 * Write the data to the modal. Either open or update.
	 * @param {string} url - URL, the data shall be requested. A Response with json is expected.
	*/
	function getRequestForModalFromURL(url) {
		$.ajax({
			type: 'GET',
			url: url,
			data: {'as': 'modal'},
			error: function(jqXHR, textStatus, errorThrown){
				var msg = 'GET-Request: Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
				console.log(msg);
				updateOrCloseModal({'modalBody': msg})
			},
			success: function(data){
				if (data.hasOwnProperty('modalBody')){
					updateOrCloseModal(data);
				} else {
					window.location.href = url;
				}
			}
		})
	}


	/**
	 * Request POST-data to the webserver for the modal.
	 * Write the answer data to the modal. Either open or update.
	 * @param {string} form_jquery_indicator - Navigation for JQuery to select the correct form.
	 * @param {string} action - Keyword, the webserver expects to process the correct modal POST request.
	 * @param {integer} id - Optional. Pass the id to the webserver with the POST-data. Pass `-1` to not pass a valid id and reach to reach the next parameter.
	 * @param {boolean} reload_on_success - Optional. If `true`, the page will be reloaded. (Important, when a POST in a Modal happens and and changes data on the main page.)
	 * @param {string} further_data_as_string - Give further POST data directly as string.
	*/
	function postRequestForModal(form_jquery_indicator, action, id, reload_on_success=false, further_data_as_string) {
		var form = $(form_jquery_indicator);
		var _data;

		_data = '&action=' + action;
		if (id != undefined && id != -1) {
			_data = _data + '&id=' + id
		}
		if (further_data_as_string != undefined) {
			_data = _data + further_data_as_string;
		}
		$.ajax({
			type: 'POST',
			url: window.location.href,
			data: form.serialize() + _data,
			error: function(jqXHR, textStatus, errorThrown){
				var msg = 'POST-Request: Es gab einen Fehler mit Fehlercode '+jqXHR.status+': "'+errorThrown+'".'
				console.log(msg);
			},
			success: function(data){
				updateOrCloseModal(data, reload_on_success);
			}
		});
	}


	/*
	 *When the modal is closed (click on close or outside the modal), the content is cleared and event handlers, added to the modal, are deleted (e.g. myFlexOffer_viewFO_Body.html with a handler to resize the plots).
	 * This is run on loading the page.
	*/
	$(document).ready(function(){
		$('div.modal').on('hidden.bs.modal', function() {
			emptyModal();
			$(document).off('shown.bs.modal');
		});
	});