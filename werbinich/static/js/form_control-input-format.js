
/**
 * Function to force a format in an form input field.
 *  Example: 
 *   The Input "AB12345CD3" becomes formated to "AB1 2345 CD3" with following function call:
 *   `controlInputFormat( $("input[name='myfield']"), 10, [3,4,3] )`
 *
 * @param {object} $input - JQuery-Object of the input field to format
 * @param {integer} max_length - Text will be cutted after `max_length` characters. Deleted characters are not counted.
 * @param {array of integer} block_lengths - Array of the block lengths. Between the blocks, there will be inserted a "divider"-character.
 * @param {boolean} toUpperCase - Default: True. When true, the input will be transformed to upper-case letters
 * @param {regex object} regex_deleted_chars - Expression of input characters to be deleted. Take care to have a global setting
 * @param {text} divider_char - Character for the gap between the blocks to be inserted. Default: Space-character
 */

		function controlInputFormat(
				$input,
				max_length,
				block_lengths,
				toUpperCase = true,
				regex_deleted_chars = /[ \.\_\-]+/g,
				divider_char = " "
		){

			function innerfunc(
				$input,
				max_length,
				block_lengths,
				toUpperCase,
				regex_deleted_chars,
				divider_char
			){
				// Get the input value
				var input = $input.val();

				// Delete spaces and connection signs
				input = input.replace(regex_deleted_chars, '');
				// Restrict to max characters
				input = input.substring(0,max_length);
				// Create the divisions into the blocks
				var d = [];
				var e = [];
				var chunk = [];

				for (var i=0; i<block_lengths.length; i++) {
					if (i==0){
						d.push(block_lengths[i]);
					} else {
						d.push( d[d.length-1] + block_lengths[i] );
					}
				}
				d.push(max_length);

				e.push(0);
				e = e.concat( d.slice(0,d.length-1) );

				for (var i=0; i<d.length; i++) {
					chunk.push( input.substring( d[i], e[i] ) );
				}

				// return the result to the input
				if (toUpperCase) {
					$input.val( function() {
						return chunk.join(divider_char).toUpperCase().trim();
					});
				} else {
					$input.val( function() {
						return chunk.join(divider_char).trim();
					});
				}
			}


			// Check, if the input object exists
			if ($input.length != 1){ return; }

			// This is the initialisation of the event for keys
			$input.on("keyup", function( event ) {

				// When user select text in the document, also abort.
				var selection = window.getSelection().toString();
				if (selection !== '') { return; }

				// When the arrow keys or other function keys are pressed, abort.
				if($.inArray( event.keyCode, [38,40,37,39,8,9,13,16,17,18,19,20,27,33,34,35,36,37,46] ) !== -1 ) { return; }

				return innerfunc(
					$input,
					max_length,
					block_lengths,
					toUpperCase,
					regex_deleted_chars,
					divider_char
				);

			});

			// This is an initial event for formatting the initial input
			return innerfunc(
				$input,
				max_length,
				block_lengths,
				toUpperCase,
				regex_deleted_chars,
				divider_char
			);

		}


