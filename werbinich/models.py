from django.db import models
import django_helpers.validate_unique


class Game(models.Model):

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	host_session_key = models.CharField(max_length=40)
	game_url = models.CharField(
		max_length=20,
		unique= True,
	)

	ts_started = models.DateTimeField(
		blank= True, null= True,
	)
	ts_ended = models.DateTimeField(
		blank= True, null= True,
	)

	class Meta:
		unique_together = ['host_session_key', 'game_url',]


	def __str__(self):
		return f'({self.id}) {self.host_session_key}: {self.ts_started} - {self.ts_ended}'


	def is_running(self):
		""" If all werbinich_names have been assigned, the game is running and function returns True

		:return: Boolean
		"""

		if not self.ts_started or self.ts_ended: return False

		players = self.player_set.all()
		return all( [x.werbinich_name is not None for x in players] )



class Player(models.Model):

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	game = models.ForeignKey(
		Game,
		on_delete= models.CASCADE,
	)

	player_session_key = models.CharField(max_length=40)
	player_name = models.CharField(max_length=64)

	werbinich_name = models.CharField(max_length=128, blank=True, null=True)

	player_to_assign = models.ForeignKey(
		'self',
		on_delete= models.CASCADE,
		null= True, blank= True,
	)

	class Meta:
		unique_together = (
			['game_id', 'player_session_key',],
			['game_id', 'player_name',],
		)


	def validate_unique(self, exclude=None):
		super(Player, self).validate_unique(exclude)

		# unique_together for clean
		django_helpers.validate_unique.validate_unique(
			self,
			game_id= self.game_id,
			player_name= self.player_name,
		)


	def __str__(self):
		return f'({self.id}) game_ID: {self.game_id}, {self.player_name} -> {self.werbinich_name}'


	def has_assigned_werbinich_name(self):
		""" If user has assigned the werbinich_name to the designated user, the function returns True.

		:return: Boolean
		"""

		if not self.player_to_assign: return False

		return self.player_to_assign.werbinich_name is not None

