import string
import random


def generateRandomString(
		length= 20,
		chars= string.ascii_lowercase + string.digits,
):
	return ''.join(
		random.SystemRandom().choice(
			chars
		) for _ in range(length)
	)